
# CaesarV: Compromised Servers Enable Traffic Deluge
##### Publication Date: 2017-09-19 00:00:00+00:00
##### Authors: []
----
Traffic is the lifeblood of the cybercrime ecosystem. It is a core commodity enabling cybercriminals to monetize their operations in various ways. For example, in our recent analysis of [a fake Russian dating scheme](https://www.riskiq.com/blog/labs/fingta-fake-dating/), we observed that fraudsters used online ads to push web traffic to their sites and hijacked web browsers with adware to redirect victim computers. Meanwhile, other actors tap into traffic for much more illicit reasons, such as hijacking traffic from large advertising networks, carrying out phishing attacks, and distributing malware to vulnerable computers. 

 Many attackers take active steps to protect this source of revenue, utilizing various [traffic and device filtering techniques](https://www.riskiq.com/blog/external-threat-management/traffic-filtering-techniques/) to block out security researchers and optimize the type of traffic they receive. In today’s post, we’ll examine a campaign RiskIQ has been tracking for several weeks involving traffic monetization and filtering, obfuscation, and several thousand compromised web servers.

 ### Initial Observations on CaesarV

 This particular campaign, which we have named CaesarV after its use of a [Caesar cipher](https://learncryptography.com/classical-encryption/caesar-cipher) to obfuscate code on its pages that cause redirection, uses spam to generate traffic. [Spam](https://www.riskiq.com/blog/external-threat-management/its-a-spam-party/) builds traffic by sending malicious URLs and attachments to a large number of contacts that may have been stolen from address books, harvested from websites, collected from data breach dumps, or purchased from various sellers and marketing database suppliers. When one of these recipients clicks the URL embedded in the spam email, they are sent to a page on a compromised web server which then distributes the traffic among several different scam pages.

 ![](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![](https://www.riskiq.com/wp-content/uploads/2017/09/rsz_2image2.png)

 Shown above is the response body from one of the CaesarV pages detected by RiskIQ. It contains a few interesting elements including the script (highlighted), which acts as a Caesar cipher. The variable castlesa=61 defines the shift for all of the numbers in the castlesb array, meaning each number in the castlesb array is a character code with 61 added to it. After deobfuscation, the character code array resolves to this redirection:

 **window.top.location.href='http://rapiseebrains.com/?a=401336&c=cpc&s=050217';**

 The result is an affiliate offer for a scam page using a fake news story to sell pills endorsed by Stephen Hawking that purport to improve intelligence.

 ![](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![](https://www.riskiq.com/wp-content/uploads/2017/09/rsz_2image1.png)

 Other observed redirections have landed on fake tech support scams, fake Flash update pages, fake diet pills, and other similarly scammy pages. Since RiskIQ began detecting this behavior, we’ve seen several thousand samples pointing to an equally high number of compromised web servers since at least the beginning of 2017—and possibly much longer. 

 ### Investigation

 The actor(s) responsible for CaesarV work by finding vulnerable servers, compromising them, and loading several files onto them, including the PHP files containing the CaesarV code. Analysis of a server hosting CaesarV showed that, following initial compromise, the file **/tmp/rnd** is written to disk and the perl process is spawned, which then removes the /tmp/rnd file. The perl process then downloads several PHP files from a remote host with the following response headers:

 ![](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![](https://www.riskiq.com/wp-content/uploads/2017/09/rsz_1image3.png)

 Following the download, the process opens listening **TCP port 23213**. Several inbound connections on this port are received from *** [* [77.72.83.83](https://community.riskiq.com/search/77.72.83.83)](https://community.riskiq.com/search/* [77.72.83.83](https://community.riskiq.com/search/77.72.83.83)),** which is running FTP on port 21, SSH on port 22, HTTP on port 81, and an unknown service on port 84.

 *** [* [77.72.83.83](https://community.riskiq.com/search/77.72.83.83)](https://community.riskiq.com/search/* [77.72.83.83](https://community.riskiq.com/search/77.72.83.83))** AS29073 | NL | QUASINETWORKS

 RiskIQ analyzed one of the PHP files loaded onto a server after initial compromise. This particular file is hardcoded to create the redirection page and attempts to perform other actions on the compromised server. The file includes an array of words from which the name of the variables in the Caesar cipher script is selected, as well as an array of numbers from which to select the shift for the character codes in the cipher. 

 There is also a function involving a cookie check and manipulation of htaccess files on the server. If the page is visited by a browser with the correct cookie set, the script will attempt to change permissions on htaccess files on the compromised server to make them writable and then delete them, possibly exposing restricted access in the server’s document root to allow access or to remove restrictions from directory paths.

 ![](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![](https://www.riskiq.com/wp-content/uploads/2017/09/rsz_1image4-1.png)

 ### A Continuing Trend

 There is [money to be made in a compromised server](https://krebsonsecurity.com/2012/10/the-scrap-value-of-a-hacked-pc-revisited/), and there are an awful lot of vulnerable servers out there waiting to be compromised. In the case of CaesarV, the value comes from the use of an enormous set of domains to which the actor can link for redirection of their spam-created traffic to scam sites. As long as there are vulnerable systems and means of monetizing their compromise, there will continue to be attacks that place them in the hands of malicious actors.

 RiskIQ monitors this type of activity on a continual basis using our web data collection platform driven by [virtual users](https://www.riskiq.com/platform/) and [URL Intelligence services](https://www.riskiq.com/products/url-intelligence/). Staying informed on the use of tactics such as these in the threat space is helpful in today’s battle to protect users and systems from external threats. 

 [RiskIQ Community project.](https://community.riskiq.com/projects/865cb11e-dd91-7370-7b58-10a4fad2e9c9)

 Questions? Feedback? Email [research-feedback@riskiq.net](mailto:research-feedback@riskiq.net) to contact our research team.

 
        