
# A Deeper Look at the Phishing Campaigns Targeting Bellingcat Researchers Investigating Russia
##### Publication Date: 2019-08-01 00:00:00+00:00
##### Authors: []
----
On July 26th, ThreatConnect published an analysis of a coordinated phishing attack against Bellingcat, an investigative journalism website that specializes in fact-checking and open-source intelligence. Known for their work investigating Russia, Bellingcat researchers were carefully chosen targets, as [stated by Bellingcat’s Eliot Higgins on Twitter](https://twitter.com/EliotHiggins/status/1155031447230722049). 

Highly focused, the phishing campaign targeted the digital security of only ten individuals, who have been [identified by investigative journalist Christo Grozev.](https://twitter.com/christogrozev/status/1155078835261624320) These include some researchers who do not work for Bellingcat but do investigate Russia.

[ProtonMail, the email service used in the phishing attack, published a short statement](https://protonmail.com/blog/bellingcat-cyberattack-phishing/), which included some fascinating details on the phishing attack from their perspective.

Introduction
------------

In this article, we’ll explore a different angle to this campaign by analyzing it from the unique outside-in perspective of RiskIQ. RiskIQ data reveals multiple phishing campaigns involving different tactics beyond the analysis by ThreatConnect. 

The earliest cyber threat activity we see for this campaign was when the cyber attackers registered [*** [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m**](https://community.riskiq.com/search/* [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m) on June 27th, 2019. They also issued a Lets Encrypt certificate on the same day which we observed on [*** [* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)))](https://community.riskiq.com/search/* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))))**](https://community.riskiq.com/search/* [* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)))](https://community.riskiq.com/search/* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))))), the same IP to which the [*** [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m**](https://community.riskiq.com/search/* [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m) domain pointed. 

We then observed the same certificate being served from [*** [* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)))](https://community.riskiq.com/search/* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))))**](https://community.riskiq.com/search/* [* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)))](https://community.riskiq.com/search/* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))))) at the beginning of July. This IP address was mainly used later in the campaign for other phishing domains (more on this in the infrastructure investigation portion of this article).

From lure to phish
------------------

The cyber attackers sent emails to a very select pool of targets indicating a possible breach of digital security and the integrity of the target’s ProtonMail account. Under the subject line, “*Someone exported your encryption keys*,” the cyber attackers sent the following message:

![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](https://www.riskiq.com/wp-content/uploads/2019/07/Webp.net-resizeimage-60.jpg)

The July emails contained links to [**mail.* [protonmail.sh](https://community.riskiq.com/search/protonmail.sh)**](https://community.riskiq.com/search/mail.* [protonmail.sh](https://community.riskiq.com/search/protonmail.sh)), with links to /password and /keys. These URLs redirected targets towards the final phishing pages on a new host, [*** [mailprotonmail.ch](https://community.riskiq.com/search/mailprotonmail.ch)**](https://community.riskiq.com/search/* [mailprotonmail.ch](https://community.riskiq.com/search/mailprotonmail.ch)). Interestingly, the exact visited URLs wouldn’t matter; the server behind [**mail.* [protonmail.sh](https://community.riskiq.com/search/protonmail.sh)**](https://community.riskiq.com/search/mail.* [protonmail.sh](https://community.riskiq.com/search/protonmail.sh)) was instructed to always 301 redirect visitors to [*** [mailprotonmail.ch](https://community.riskiq.com/search/mailprotonmail.ch)**](https://community.riskiq.com/search/* [mailprotonmail.ch](https://community.riskiq.com/search/mailprotonmail.ch)):

![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](https://www.riskiq.com/wp-content/uploads/2019/07/Webp.net-resizeimage-59.jpg)

The final landing page was a copy of the real ProtonMail app. We can see they didn't depend on the caching mechanism ProtonMail has in place typically, but all other resources will match up perfectly:

![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](https://www.riskiq.com/wp-content/uploads/2019/07/Webp.net-resizeimage-2019-07-31T192913.393.png)

The design of the phishing page shown to the visitor matched the ProtonMail app login prompt:

![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](https://www.riskiq.com/wp-content/uploads/2019/07/Webp.net-resizeimage-2019-07-31T192931.153.png)

As we stated at the beginning of this article, the recent coverage looked explicitly at the July campaign and its activity and associated infrastructure. We can confirm these spear-phishing attacks have been happening since late June.

On June 27th, we observed the above registration, for [*** [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m**](https://community.riskiq.com/search/* [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m), which the cyber attackers used in earlier spear-phishing emails. The breach in digital security and lure in these emails was slightly different from the July campaign: simple log in pages. The emails contained links to /login on the [*** [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m**](https://community.riskiq.com/search/* [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m).

We observed our first hits for these phishing emails soon after the domain was set up. Here is a crawl from the end of June:

![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](https://www.riskiq.com/wp-content/uploads/2019/07/Webp.net-resizeimage-2019-07-31T192944.866.png)

The setup for this phishing page was the same as the later cyber attacks in July, the lure sent to the targets was just slightly different. The targets identified in the July cyber attacks might be part of a broader set, but we do not think it will expand beyond individuals who investigate Russia.

There was another campaign before the June and July campaigns using phishing pages hosted through [**my.* [secure-protonmail.com](https://community.riskiq.com/search/secure-protonmail.com)**](https://community.riskiq.com/search/my.* [secure-protonmail.com](https://community.riskiq.com/search/secure-protonmail.com)). This continuation of phishing attempts tells us the cyber attackers were highly focused on their targets. The duration of their campaign might also tell us they weren't quite finished.

Associated Infrastructure (IOCs)
--------------------------------

While there are a lot of suspected infrastructure points to this group, including those used in other cyber threat campaigns, we will only list the infrastructure confirmed by RiskIQ crawlers. 

Additionally, all infrastructure pieces provided here are also available in a RiskIQ Community project (no need to register or authenticate) here:

<https://community.riskiq.com/projects/c0975eea-b821-07d5-a20e-da04b6758bf7>

The earliest IP infrastructure we have pinned down this campaign was in April when [*** [* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)))](https://community.riskiq.com/search/* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))))**](https://community.riskiq.com/search/* [* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)))](https://community.riskiq.com/search/* [* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))](https://community.riskiq.com/search/* [* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199)](https://community.riskiq.com/search/* [193.33.61.199](https://community.riskiq.com/search/193.33.61.199))))) started to be used for a few domains:

![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](https://www.riskiq.com/wp-content/uploads/2019/07/Webp.net-resizeimage-2019-07-31T193004.174.png)

The second IP address [*** [* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)))](https://community.riskiq.com/search/* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))))**](https://community.riskiq.com/search/* [* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)))](https://community.riskiq.com/search/* [* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))](https://community.riskiq.com/search/* [* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249)](https://community.riskiq.com/search/* [217.182.13.249](https://community.riskiq.com/search/217.182.13.249))))) started to get used by the actors between half of June into early July:

![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![In this article, we’ll explore a different angle to the phishing campaign against Bellingcat by analyzing it from the outside-in perspective of RiskIQ.](https://www.riskiq.com/wp-content/uploads/2019/07/Webp.net-resizeimage-2019-07-31T193017.755.png)

The domains registered by the attackers tell a different story, however. Domain infrastructure was being registered back in March of this year. Here is the full set of associated domains sorted on registration/creation date with the registrar used:

**Domain****Registration date****Registrar*** [mailprotonmail.ch](https://community.riskiq.com/search/mailprotonmail.ch)

2019-07-22

epag domainservices gmbh

* [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)

2019-07-22

tucows domains inc.

* [mailprotonmail.co](https://community.riskiq.com/search/mailprotonmail.co)m

2019-06-27

tucows domains inc.

* [protonmail.sh](https://community.riskiq.com/search/protonmail.sh)

2019-06-27

epag domainservices gmbh

* [prtn.app](https://community.riskiq.com/search/prtn.app)

2019-06-27

tucows domains inc

* [protonmail.direct](https://community.riskiq.com/search/protonmail.direct)

2019-06-21

tucows domains inc.

* [protonmail.gmbh](https://community.riskiq.com/search/protonmail.gmbh)

2019-06-21

tucows domains inc.

* [protonmail.systems](https://community.riskiq.com/search/protonmail.systems)

2019-06-21

tucows domains inc.

* [protonmail.support](https://community.riskiq.com/search/protonmail.support)

2019-05-06

tucows domains inc.

* [protonmail.team](https://community.riskiq.com/search/protonmail.team)

2019-05-06

tucows domains inc.

* [protonmail.earth](https://community.riskiq.com/search/protonmail.earth)

2019-04-22

web4africa inc.

* [secure-protonmail.com](https://community.riskiq.com/search/secure-protonmail.com)

2019-04-11

web4africa inc.

* [prtn.xyz](https://community.riskiq.com/search/prtn.xyz)

2019-04-11

web4africa inc.

* [prtn.email](https://community.riskiq.com/search/prtn.email)

2019-03-12

tucows domains inc.

  
Additionally, the cyber attackers make use of the free Let's Encrypt certificate service. You can track parts of the infrastructure by looking up the domain names against our expansive SSL certificate database. For example, here are two serial numbers for two of the domains that can be tracked through infrastructure (serial numbers will not be updated/changed for extending certificates with Let's Encrypt):

<https://community.riskiq.com/search/certificate/serialNumber/287364094689339033798171303978159330084687>

<https://community.riskiq.com/search/certificate/serialNumber/285200805608237752767204219352640170865>

<https://community.riskiq.com/search/certificate/serialNumber/430867895058561196482072155366212987335283>

 
        