
# A Look Back on Phishing Attacks in 2016
##### Publication Date: 2017-02-03 00:00:00+00:00
##### Authors: []
----
Phishing attacks were on the rise in 2016. Whether you’re a national political organization, a Fortune 500 company, or a small business, if you have a digital presence, your brand, customers, and employees are targets for phishing.

 At RiskIQ we process tons of web-related threat data. One of the types of data we look at is phishing incidents. From various sources, we receive URLs which might be indicative of phishing. The URLs are processed through our crawling infrastructure and fed through our machine-learning technology to classify each detected phishing page appropriately.

 Within this group of phishing pages, there are those used for highly targeted phishing attacks, also known as ‘spear phishing,’ as well as phishing pages used for widespread 'generic' phishing. Regarding infrastructure, there are two distinctions: self-maintained custom infrastructure and abused or compromised infrastructure belonging to someone else. We'll explore both of these throughout this post, as well as the evolving tactics of phishing campaigns. 

 ### Statistics

 To give an idea of the amount of data we processed for phishing alone, here's an overview of what 2016 looked like in our system for phishing incidents only. The graph below totals to approximately 58 million incidents throughout 2016, or 158,904 a day:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/phishing1.png)

 ### Affected Hosting Organizations

 Most phishing pages come from compromised websites. The web hosters involved with most of these websites haven't changed over the past few years—typically, the largest web hosts most often associate with phishing. 

 Topping these rankings, we have HostGator. In 2nd place, we have GoDaddy, another massive and popular hosting company. We don't have clear insights into why these companies are implicated in so many phishing attacks (other than being two of the largest hosting organizations, of course), but there are two questions to which we would need the answers to find out:

 
 2. Are all affected websites maintained by the customers or the hosting organization?


 4. Are the websites we have observed directly hosted by these two companies or by resellers on their platform?


 
 Without correlating the above questions, it's hard to point any fingers at specific hosts or their resellers and customers for lacking a proper upgrade/update process.

 TLD & gTLD Insights

 One fascinating thing to look at is the top-level domain (TLD) distribution in the observed phishing websites. The top five is not very special in and of itself, but distribution wise, it is interesting to note the dominance of ".com" as well as the closest runner-up being the Brazilian "com.br".

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing2.png)

 Taking an even closer look, a new angle develops: cyber threat actors disproportionately target specialized government TLDs, such as ".gov" domains. Based on the data, we can say that the following countries have some improvements to make on the IT side of their government:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing3-1.png)

 An interesting note: something that has been widely abused in the field of exploit kits and malware command and control servers is generic TLDs (gTLDs). Some gTLDs are very cheap to register, which makes them extremely useful for criminals. However, on the phishing side, we have seen a very limited amount of use for this; the numbers are negligible. 

 ### Phishing Examples

 In RiskIQ's phishing data, we mostly get phishing pages focused on impersonating financial institutions. When digging through the lower-count phishing campaigns and incidents, however, there are some rather intriguing cases.

 #### Targeted phishing towards healthcare

 We saw a small section of what appears to be a campaign targeting healthcare companies. Because the campaigns were so targeted, we had a limited view, but we submitted the data to our systems for processing (the bad guys won't do it for us, sadly). Most of the examples belong to campaigns targeting the company’s' Outlook Web Access portal, where employees read their email. Below is an example targeting a large healthcare network:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing4-1.png)

 Below, another health organization was targeted by this same actor:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing5-1.png)

 This phishing page looks generic, but the phish URL shows that this organization was deliberately targeted: http://wrn.fridayflowers.com.au/outl/[brand obfuscated]/logon-aspx.

 The phishers here are going for breaching these healthcare providers, possibly with a financial gain in mind. Eventually, they can profit by selling access, PII, and other sensitive data. 

 #### eGovernment accounts

 A popular theme of phishing for various governmental service accounts arises around the time citizens have to enter their tax forms. In the United States, the phishing pages for the IRS are extremely common, but UK citizens are targeted as well. The fake pages below claim to be from the UK's *Revenue & Customs* organization and ask for a very extensive list of information that would allow the cyber attacker to take complete control of the victim's sensitive data:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing_6-1.png)

 The same scheme also occurs in France around tax time. These pages mimic the 'Impots' website of the French government:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing7-1.png)

 #### WhatsApp 'phishing'

 Another campaign that our system encounters are phishing pages aimed at WhatsApp users. This technique doesn’t phish for any credentials; it goes for direct profit. These pages are more 'scam’ websites than phishing pages, but the tactics employed are the same as phishing. The users are lured into paying fake subscription costs for the continued use of their WhatsApp.

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing8-1.png)

 The actors mostly try to convince users by [domain shadowing](https://www.riskiq.com/blog/external-threat-management/domain-shadowing-good-domains-go-bad/) legitimate domains, as seen in a small section of our hits:

 http://whatsapp-messenger.whatsapp.com-contact.playatsapp.com/

 http://whatsapp-messenger.whatsapp.com.whatsapp.playatsapp.com/

 http://whatsapp-tecnico.supporto-ufficiale1.wetips.net/

 #### Cyber Security vendor name abuse

 Another ongoing trend is the use of known internet security vendor names on phishing pages. While the "Your machine is infected, pay now to clean up" pages do go for your money, there are some pages out there that phish for email credentials. One example is the use of Symantec as a lure in the phishing page below:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing9-1.png)

 #### Unwanted ride sharing

 Occasionally, phishing pages for Uber appear in our data. The pages themselves are just classic phishing pages with reconstructed Uber portal pages:

 ![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Phishing attacks were on the rise in 2016. If you have a digital presence, your brand, customers, and employees are targets for phishing.](https://www.riskiq.com/wp-content/uploads/2017/02/rsz_phishing10-1.png)

 ### Conclusion

 We just looked at just a few of the curious phishing examples we see going around every day. As you can see, criminals are always innovating and creating new methods to lure in more victims and gain access to their financial information, PII, and user accounts. Criminals will use any angle to get their victims to bite. 

 By tracking against a wide range of sources—social media, digital ads, known phish events within the RiskIQ index, Domain-based Message Authentication, Reporting and Conformance (DMARC), abuse box and referrer log integrations for known phishing signatures, and 15 reputational list sources—RiskIQ provides accurate, comprehensive coverage against rapidly growing phishing threats. With more than 30 million phishing pages already scanned, and tens of thousands of new pages scanned each day, we understand how best to identify phishing campaigns and mitigate their impact. 

 As RiskIQ collects more data, our Cyber Threat Research Team will be sure to update you with more articles like this one.

 
        