
# Are Cyber Security Vendors Using Scareware Traffic-delivery Tactics?
##### Publication Date: 2016-03-17 00:00:00+00:00
##### Authors: []
----
While surfing the internet with a mobile device, we've observed advertisements by [Moiety Media](http://www.moietymedia.com/) serving legitimate antivirus (AV) software ([Qihoo](https://www.360totalsecurity.com) 360 security for mobile) to the public via Scareware techniques. This delivery method is concerning because it's scaring people into downloading legitimate AV software from Google Play directly from affiliate download links—a method of traffic delivery that was abused heavily in the past to deliver fake AV software. Those campaigns were very lucrative, which may be the motivation for this trending type of advertising.

 ![Screen Shot 2016-09-09 at 10.26.21 AM](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Screen Shot 2016-09-09 at 10.26.21 AM](https://www.riskiq.com/wp-content/uploads/2016/03/Screen-Shot-2016-09-09-at-10.26.21-AM.png)

 Many advertisers are using this technique, but in this specific example, the advertising company behind these tactics is Moiety Media. Based on our review, Moiety Media appears to be a vendor used to market to specific targeted audiences; in this case, mobile users. Based on their other listed projects (fig1-2), a quick cross reference with Google Play, and the amount of downloads for 360 security, Moiety Media’s advertising has been successful in the mobile realm.

 ![Screen Shot 2016-09-09 at 10.27.29 AM](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Screen Shot 2016-09-09 at 10.27.29 AM](https://www.riskiq.com/wp-content/uploads/2016/03/Screen-Shot-2016-09-09-at-10.27.29-AM.png)

 **Why is this happening and why has no one addressed these methods?**

 Potentially, it's due to the lack of verification of affiliate activity—if the affiliates are the responsible party. The legitimate cyber security vendors may not be vetting these low-quality traffic methods before the ads are published.

 Additionally, we could be seeing a form of the activity we saw during the [FakeAV](https://www.scmagazineuk.com/fake-av-apps-spotted-google-play-windows-phone-store/article/1481142) campaigns in the past. In these campaigns, the code was first submitted to the advertising companies for a review before publication. After passing inspection, the ads were altered for nefarious purposes and re-uploaded, masquerading as the vetted advertisement.

 Whatever the case, it appears that nothing is being done to thwart this behavior. Therefore, one can assume that apathy is the main reason there are so many downloads of the software.To put this problem in perspective, we researched millions of downloads resulting from this distribution method, an indication that these tactics are widespread and highly successful.

 **Why are AV Vendors using affiliate programs when they can handle advertising themselves?** 

 A good reason for having an affiliate program is to reach some avenues in which the AV vendor is not good at, or used to, advertising.This activity has been observed within erotic advertisements, pirated software sites, and other locations in which one wouldn't expect to see advertising for legitimate cyber security peddled.

 **Conclusion**

 This observation raises some compelling Questions. Are these the same actors previously seen with FakeAV methods, who have moved on to distributing real AV software for financial gain? Is this simply abuse by AV vendors that pay out to affiliates per install? Could it be that AV vendors are purchasing this low-quality traffic themselves to get more downloads? The answer could be none—or all—of the above.

 Cyber security vendors should vet their affiliates and advertising partners more thoroughly and make sure they understand their advertising methods. We'll continue to monitor this trend and report noteworthy findings of Scareware techniques and other fraudulent activity.

 
        