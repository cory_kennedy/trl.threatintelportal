
# Cyber Threat Hunting Close to Home: Escaping the RIG EK with Passive DNS
##### Publication Date: 2016-11-11 00:00:00+00:00
##### Authors: []
----
Sometimes cyber threat hunting brings you to familiar territory.

 RiskIQ collects data that spans the entire internet—our network of sensors gather a massive repository of data sets such as passive DNS (PDNS) and WHOIS. But even though we're hunting cyber threats all over the world, sometimes compromises hit a little close to home. Today's post is a great example.

 Near the RiskIQ office in Kansas City, locals frequent an ‘[escape room](https://en.wikipedia.org/wiki/Escape_room)’ business for team building and fun nights on the town. And while hunting cyber threats for one of our clients, a hostname we identified serving RIG exploit kit (EK) looked a bit familiar–it belonged to the very same local escape room business. You can see in the RiskIQ web crawl below that the owner of the website, a roger@ticktockholdings.com, appears to have fallen victim to [domain shadowing](https://www.riskiq.com/blog/external-threat-management/domain-shadowing-good-domains-go-bad/).

 Cyber threat actors have compromised his GoDaddy account and created a new A record to which to redirect traffic. The new site, which the registrant probably has no idea exists, was shadowing the reputable domain of the escape room business and hosting the very dangerous RIG EK.  
 

 ![escape1](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![escape1](https://www.riskiq.com/wp-content/uploads/2016/11/ESCAPE1.png)

 Legitimate domains of businesses like this one are particularly attractive to cyber threat actors because they have a good reputation on the Internet. That means they won't raise any red flags to most cyber security teams and perimeter controls will usually allow the traffic. Once actors compromise the registrant’s credentials of a valuable domain, they'll create malicious A records on some or all of the domains the registrant owns, increasing the scale of their infrastructure. 

 Malicious A records from domain shadowing are so difficult to detect because they're mixed in with good pages on otherwise reputable parent domains, and thus, prove to be a very useful weapon in a cyber criminal’s arsenal. As you can see in the screenshot below from PassiveTotal, RiskIQ's free cyber threat research tool, the cyber threat actors have gone through and added new A records to the registrant’s existing domains with which to host RIG EK.

 [![escape2](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![escape2](https://www.riskiq.com/wp-content/uploads/2016/11/escape2.png)](https://www.passivetotal.org/whois/email/roger@ticktockholdings.com)

 **Stay safe out there**

 Layering PDNS data over WHOIS lookups tells a detailed story about the cyber threat actors behind domain shadowing and allows multiple pivot points to get the to the data that cyber security teams need. PDNS is a system of record that stores DNS resolution data for a given location, record, and period. This historical resolution data set allows analysts to view which domains resolved to an IP address and vice versa, as well as time-based correlation of domain or IP overlap.

 Here, inside PassiveTotal, we can pivot on the PDNS heat map see the new subdomain resolving to a brand new IP on October 26th, the same day as the cyber attack.

 [![](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![](https://www.riskiq.com/wp-content/uploads/2016/11/escape3.png)](https://www.passivetotal.org/passive/add.ESCAPEGAME-KC.COM)

 Because RiskIQ has one of the largest repositories of PDNS data in the world and the unique ability to correlate that data, we use it to determine when a new subdomain is first observed and analyze all newly observed hosts in much the same way that monitoring new WHOIS registrations can provide this information for parent domains. 

 Cyber threats like this have become common and ubiquitous around the world. And as you can see, they affect businesses of all types, no matter the size or industry.

 
        