
# New Year, Same Magecart: The Continuation of Web-based Supply Chain Attacks
##### Publication Date: 2019-01-16 00:00:00+00:00
##### Authors: []
----
RiskIQ has tracked [Magecart](https://www.riskiq.com/blog/category/magecart/) and exposed their attacks for years. Now, the term is top-of-mind in the security community and beyond, with a Google search of ‘Magecart’ returning over 170,000 results. In fact, the cybercriminal group of digital credit card-skimming gangs gained such notoriety throughout last year that [WIRED named Magecart](https://twitter.com/WIRED/status/1079380603752116224) in its list of “[Most Dangerous People On The Internet In 2018](https://www.wired.com/story/most-dangerous-people-on-internet-2018/).” 

With the threat of Magecart looming large, RiskIQ receives a continuous flow of questions from businesses looking to protect their attack surface; law enforcement tracking each Magecart group, reporters covering Magecart activity, and other vendors looking to leverage RiskIQ’s unique web forensics data which enabled us to disclose Magecart attacks against [Ticketmaster](https://www.riskiq.com/blog/labs/magecart-ticketmaster-breach/), [British Airways](https://www.riskiq.com/blog/labs/magecart-british-airways-breach/), [Newegg](https://www.riskiq.com/blog/labs/magecart-newegg/), and more.

Unfortunately, Magecart is only becoming a more significant threat as it scales and evolves faster than ever, but we will continue to track Magecart activities and new groups as they emerge. This report details another attack campaign occurring over the past months that used a third-party supply chain attack, a tried and true Magecart tactic used in Group 5’s breach of [Ticketmaster](https://www.riskiq.com/blog/labs/magecart-ticketmaster-breach/).

Web-based supply chain attacks compromise vendors that supply code often used to add or improve site functionality. This code integrates with thousands of websites, so when it’s compromised, the sites of all of the customers that use it are compromised. This gives Magecart access to a wide range of victims at once.

#### Introduction

On November 18th, 2018 we released a major report titled “[Inside Magecart](https://www.riskiq.com/research/inside-magecart/),” which broke down the operational side of seven individual Magecart groups, as well as each group’s history. One of the most successful groups we profiled was Magecart Group 5, which gained the broadest reach of all the groups by performing web-based supply chain attacks on websites. 

In that report, we disclosed 12 individual third parties that were victimized as a delivery method for Group 5’s skimming code. In this report, we will add the thirteenth to that list despite the compromise coming from a different, entirely new group.

This web supply chain attack was not the work of Group 5 but instead seemed to have been a lucky “accident” by a never-before-documented group that previously only performed direct compromises. In this case, the group compromised a content delivery network for advertisements to include a stager containing the skimmer code so that any website loading script from the ad agency's ad tag would inadvertently load the Magecart skimmer for visitors. 

To the group’s delight, this content delivery injection expanded their reach, and greatly so. RiskIQ has confirmed hundreds of victim websites so far with the potential for thousands more given the number of sites running the ad tag. 

Say Hello to Magecart Group 12.

#### Direct compromise to supply-chain

Group 12 built out its infrastructure in September 2018; domains were registered, SSL certificates were set up through LetsEncrypt, and the skimming backend was installed. Group 12 doesn’t just inject the skimmer code by adding a script tag—the actors use a small snippet with a base64 encoded URL for the resource which is decoded at runtime and injected into the page. 

Below is an example from November 2018 on a website victimized by Group 12 called jadebloom.com. In RiskIQ’s web crawl, the response from the server contained this small script tag:

![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](https://www.riskiq.com/wp-content/uploads/2019/01/Webp.net-resizeimage-51.png)This small script snippet adds a new script node to the DOM which loads a script from the URL decoded in the snippet. The location for this script was:

https://content-delivery.cc/latest-version/* [content.js](https://community.riskiq.com/search/content.js)

This resource contained the skimmer which, as usual with Magecart, was obfuscated to hide its purpose:

![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](https://www.riskiq.com/wp-content/uploads/2019/01/Webp.net-resizeimage-50.png)These types of compromises continued throughout 2018 and into 2019. However, at the end of 2018, Group 12 managed to compromise a website that sent their activities into overdrive: Adverline.  


Adverline is a French advertising agency that Group 12 used as a mechanism for widespread delivery of its skimmer code. At the end of December 2018 we observed an injection on an Adverline ad tag script:

![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](https://www.riskiq.com/wp-content/uploads/2019/01/Webp.net-resizeimage-55.png)In and of itself, this is a relatively small script. Here is a cleaned up version:

![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](https://www.riskiq.com/wp-content/uploads/2019/01/Webp.net-resizeimage-54.png)This script was similar to the script seen in Group 12’s first compromises, just obfuscated slightly using javascriptobfuscator.com, a popular obfuscation service with skimmer groups.  


Researchers from Trend Micro reached out to Adverline after also discovering this hit in their telemetry earlier this month and [have also published an analysis of this group on their blog](https://blog.trendmicro.com/trendlabs-security-intelligence/new-magecart-attack-delivered-through-compromised-advertising-supply-chain/). As of this publication date, there has not been a response from Adverline to Trend Micro’s inquiry and the injections are still live. However, Trend Micro also reached out to CERT La Poste in France who did acknowledge the report and will act to remediate.

#### A new twist on an existing skimmer

The skimmer code for Group 12 has an interesting twist; it protects itself from deobfuscation and analysis by performing an integrity check on itself. The actual injection script comes in two stages, which both perform a self-integrity check. The way this happens is as follows:


* The entire first stage is executed as a function, which is globally defined (this is important)


* The second stage is encoded and padded with junk data, which is written into a new div object


* This second-stage data is then unpadded, decoded, and eventually executed


* The executed code grabs a reference to itself (which was globally set) and is put through a hashing function which is the JavaScript implementation of 

[Java’s hashCode](https://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/).


* If the check validates the next stage is executed.



This is the implementation as seen in the skimmer, which includes the check-for execution:

![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](https://www.riskiq.com/wp-content/uploads/2019/01/Webp.net-resizeimage-52.png)##### Stage 1: Fingerprinting

Although the integrity check is performed twice, the evaluated code is different each time. The first time it runs it will decode what we call the “fingerprinter” stage. This performs checks to ensure the session belongs to a legitimate consumer and not automated scanners or analysts performing a live analysis.

The fingerprinter stage contains the following mechanisms:


1. Check if it is a mobile device or not (Android, iOS variants, BlackBerry, Windows Phone)


2. Check if there is any kind of special handler in place to catch certain events such as Resizing of the window, Debugging or Console logging.


3. Put in place a function called every 500ms to clear the console for any logs created by the handler checking in the previous step.



After fingerprinting checks validate, a new script is included in the page which contains the second self-integrity check. The script is included from the first stage like this:

![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==)![Web-based supply chain attacks compromise third-party code used to add or improve site functionality. Magecart Group 12 are the latest to employ the tactic. ](https://www.riskiq.com/wp-content/uploads/2019/01/Webp.net-resizeimage-53.png)If the checks don’t go through, the code will actually remove all the artifacts (the included stage 1 script) from the page itself to clean up its traces instead of adding the 2nd stage. The 2nd stage, after performing the same self-integrity check as stage 1, will contain the actual skimmer code. 

##### Stage 2: The skimmer

The skimmer is fairly straightforward to the usual process of skimming payment information as seen with other Magecart groups. However, there are some unique angles to this group’s skimmer which we will discuss in the next sections.  
  
Data exfiltration is performed through a URL-encoded POST request which has the stolen information base64 encoded into the body.

###### Internationalized payment page keyword checking

The majority of skimmer scripts will perform a page URL check with a set of keywords before activating the actual skimming part of its code. While in most cases these keywords are English and very generic, Group 12 has added some localization by adding French and German keywords. The most likely explanation for this is the compromise of Adverline, shown above, which is a French company with a European-focused clientele.

This is the current keyword list used by Group 12 (red words are French, blue are German):


* onepage


* checkout


* ore


* cart


* pay


* panier


* kasse


* order


* billing


* purchase


* basket


* ymix


* paiement



###### Localstorage used for temporary storage

While the skimmer is obtaining the payment and billing information from victims, it will use the [localstorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) capacity of the visitor’s browser to store the information. While this is not anything particularly advanced or not seen before, it is noteworthy that Group 12 completely moved towards using localstorage instead of global variables.  
  
The used storage item names are:


* “

E-Tag

” - This contains an identifier for the skimmed user


* “

Cache

” - The payment and billing information 



#### Indicators of Compromise (IOCs)

The following RiskIQ Community project contains the IOCs associated with this Group 12’s operation. It includes the domains the group used for injecting the skimmer code into the victimized websites and receiving the stolen payment information.  
  
<https://community.riskiq.com/projects/b959cb28-d99c-b27b-bfd8-ae15d60a7e1b>

As per our usual actions when we report on an attack publicly, we've made attempts to have the domains involved taken down to stop the documented attacks. The domains have stopped functioning due to their DNS records changing, and the injections are now defunct. However because the registrar hasn't responded to our takedown requests as of this writing, we do not know if the attackers still have control of the domains to continue the attack later.

The process of taking down and/or sinkholing the domains has once again been taken up by [AbuseCH](https://abuse.ch/) and [ShadowServer](https://www.shadowserver.org/). A big thank you to these organizations for their continued support in the fight against Magecart-related activities.

 
        